
import Todo from './components/Todo.vue'
import addItem from './components/addItem.vue'
import Lista from './components/Lista.vue'

export const routes = [
    { name: 'root', path: '/', component: Lista },
    { name: 'novo', path: '/novo', component: addItem },
    { name: 'tarefa', path: '/tarefa/:titulo', component: Todo, props: true }
]
