import Vue from 'vue'
import App from './App.vue'
import "../src/assets/css/argon-dashboard.css"
import { store } from './store/store.js'
import VueRouter from 'vue-router'
import { routes } from './routes.js'

Vue.use(VueRouter);

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
